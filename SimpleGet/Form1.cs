﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace SimpleGet
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async  void button1_Click(object sender, EventArgs e)
        {
            var BaseLines = new List<string>();
            var BaseLinesInc = new List<string>();
            BaseLines.AddRange(mmoResults.Lines);

            for(int i = 0; i < 4; i++)
            {
                if (i == 0)
                {
                    BaseLinesInc.AddRange(BaseLines);
                }
                else if (i == 1)
                {
                    foreach (var line in BaseLines)
                    {
                        BaseLinesInc.Add(string.Format("{0}&s=60", line));
                    }                    
                }
                else if (i == 2)
                {
                    foreach (var line in BaseLines)
                    {
                        BaseLinesInc.Add(string.Format("{0}&s=120", line));
                    }
                }
                else if (i == 3)
                {
                    foreach (var line in BaseLines)
                    {
                        BaseLinesInc.Add(string.Format("{0}&s=180", line));
                    }
                }

            }


            while (BaseLinesInc.Count > 0)
            {
                var scrapeList = new List<string>();
                int BatchAmount = 50;
                scrapeList = BaseLinesInc.Take(BatchAmount).ToList();
                if (BaseLinesInc.Count >= BatchAmount)
                    BaseLinesInc.RemoveRange(0, BatchAmount);
                else BaseLinesInc.RemoveRange(0, BaseLinesInc.Count);
                var allResultsX = await Task.WhenAll(scrapeList.Select(itm => getWebPage(itm)));
                Debug.WriteLine("*** Writing files ***");
                foreach (var rslt in allResultsX)
                {
                    if (!rslt.Success) // if failed, try again....
                        BaseLinesInc.Add(rslt.URL);
                }
            }

            //foreach (var item in mmoResults.Lines)
            //{
            //    var rsltStr = await getWebPage(item);
            //    var fileName = string.Format("{0}\\output\\{1}{2}", AppDomain.CurrentDomain.BaseDirectory, Guid.NewGuid().ToString(), ".html");
            //    File.AppendAllText(fileName, rsltStr);
            //}
            MessageBox.Show("Complete!");
  
        }

        private async Task<ScrapeResult> getWebPage(string UR)
        {
            var rslt = new ScrapeResult();
            rslt.Success = false;
            rslt.URL = UR;
            var page = "error";
            // setup the http client
            HttpClient client =
                new HttpClient() { MaxResponseContentBufferSize = 10000000 };
            var timeout = new TimeSpan(0, 5, 0); // 5 min timeout
            client.Timeout = timeout;
            // format the URL
            Uri URL = new Uri(UR); // nb: http required for valid URL
            try
            {
                try
                {
                    Debug.WriteLine("Get: " + UR);
                    var resX = await client.GetAsync(URL);
                    page = await resX.Content.ReadAsStringAsync();
                    rslt.Success = true;
                    Debug.WriteLine("Data: " + UR);
                }
                catch (Exception ex)
                {
                    rslt.Success = false;
                    Debug.WriteLine("Error getting: " + UR);
                    page = "Error getting: " + UR 
                            + Environment.NewLine + ex.Message;
                }
            }
            finally
            {
                if (rslt.Success) { 
                var fileName = string.Format("{0}\\output\\{1}{2}", AppDomain.CurrentDomain.BaseDirectory, Guid.NewGuid().ToString(), ".html");
                File.AppendAllText(fileName, page);
                }
            }
            return rslt;
        }

        private async Task<string> getWebPage()
        {
            // setup the http client
            HttpClient client =
                new HttpClient() { MaxResponseContentBufferSize = 10000000 };
            var timeout = new TimeSpan(0, 5, 0); // 5 min timeout
            client.Timeout = timeout;
            // format the URL
            string UR = txtUrl.Text.Trim();
            Uri URL = new Uri(UR); // nb: http required for valid URL
            var resX = await client.GetAsync(URL);
            var page = await resX.Content.ReadAsStringAsync();
            return page;
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            mmoResults.Text = await getPDF();
        }

        private async Task<string> getPDF()
        {
            // setup the http client
            HttpClient client =
                new HttpClient() { MaxResponseContentBufferSize = 10000000 };
            var timeout = new TimeSpan(0, 5, 0); // 5 min timeout
            client.Timeout = timeout;
            // format the URL
            string UR = tbPDFLink.Text.Trim();
            Uri URL = new Uri(UR); // nb: http required for valid URL
            // get the base URL (this contains the link for the PDF file to download)
            var serverResponse = await client.GetAsync(URL);
            var pageData = await serverResponse.Content.ReadAsStringAsync();
            // use HTMLAgilityPack to PARSE the HTML efficently
            HtmlAgilityPack.HtmlDocument DocToParse = new HtmlAgilityPack.HtmlDocument();
            DocToParse.LoadHtml(pageData);
            // get the HREF link to the PDF file itself .. in this case, identified by the class 'product_sheet'
            var node = DocToParse.DocumentNode.SelectSingleNode("//a[@class='product_sheet']");
            var pdfLink = node.Attributes["href"].Value;
            var pdfPageLink = string.Format("{0}://{1}{2}", serverResponse.RequestMessage.RequestUri.Scheme, serverResponse.RequestMessage.RequestUri.Host, pdfLink);
            Uri URL_PDF = new Uri(pdfPageLink); // nb: http required for valid URL
            // download the PDF itself as a binary stream
            serverResponse = await client.GetAsync(URL_PDF);
            // read into a byte array
            var pdfData = await serverResponse.Content.ReadAsByteArrayAsync();
            // create a file name to save into (temp - you need to create your own format)
            var fileName = string.Format("{0}\\{1}{2}", AppDomain.CurrentDomain.BaseDirectory, Guid.NewGuid().ToString(), ".pdf");
            // use the stadard FILE utils to stream the PDF form memory to a file
            File.WriteAllBytes(fileName,pdfData);
            // now lets parse the text out of the PDF using iTextSharp library
            var pdfText = new StringBuilder();
            using (var pdfReader = new PdfReader(fileName))
            {
                // Loop through each page of the document
                for (var page = 1; page <= pdfReader.NumberOfPages; page++)
                {
                    ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();

                    var currentText = PdfTextExtractor.GetTextFromPage(
                        pdfReader,
                        page,
                        strategy);

                    currentText =
                        Encoding.UTF8.GetString(Encoding.Convert(
                            Encoding.Default,
                            Encoding.UTF8,
                            Encoding.Default.GetBytes(currentText)));

                    pdfText.Append(currentText);
                }
            }

            var allText = pdfText.ToString();
            Regex regex = new Regex(@"(\r\n|\r|\n)+");
            allText = regex.Replace(allText, Environment.NewLine);
            return allText;
        }

    }

    class ScrapeResult
    {
        public bool Success { get; set; }
        public string URL { get; set; }
    }

}
