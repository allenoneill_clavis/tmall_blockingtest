# Backgorund #
Team India reported a serious and urgent problem with blocking of scrapers from TMall in China.
This code was used to test the blocking.

Copy of report email for information/usage:

Hi all,

this is an update on progress today with TMall China blocking.

I worked with Rinal most of the day from early Irish time, and we made some interesting progress.

First of all I wanted to understand the exact nature of what was happening. Rinal reports that getting individual product detail pages is not (currently) a problem. However, doing searches have become a big problem very recently, so..

Search = Problem
Search & Detail = problem getting the top level search
Detail = not a problem (but need to discover product detail URLs at top level first for new ones at least)

My own initial investigations show that TMall has actively stepped up its blocking of spiders/robots. The first sign of this was when I tried to login as a human with a browser, I was redirected to a registration page as follows (note bold and red). Rinal has also noted this behaviour.


https://login.tmall.com/?from=sm&redirectURL=https%3A%2F%2Fsec.taobao.com%2Fquery.htm%3Faction%3DQueryAction%26event_submit_do_login%3Dok%26smApp%3Dtmallsearch%26smPolicy%3Dtmallsearch-product-anti_Spider-html-checklogin%26smCharset%3DGBK%26smTag%3DMTc2LjEyLjEwNy4xMzksLDNiZDE3OThmZTMwMDQzOWQ4MDc3NzNlYjJiZDczMGRl%26smReturn%3Dhttps%253A%252F%252Flist.tmall.com%252Fsearch_product.htm%253Fq%253D%2525B2%2525C1%2525CD%2525B7%2525B7%2525A2%2525CB%2525D9%2525B8%2525C9%2525C3%2525AB%2525BD%2525ED%2525B3%2525AC%2525C7%2525BF%2525CE%2525FC%2525CB%2525AE%2525B8%2525C9%2525B7%2525A2%2525BD%2525ED%2526type%253Dp%2526vmarket%253D%2526spm%253D875.7931836%25252FA.a2227oh.d100%2526from%253Dmallfp..pc_1_searchbutton%26smSign%3DMpjLfb6yJ6tWJGLz0yFGhA%253D%253D


Through using my VPN (Anil purchased a copy for the Ahmedabad office today), I was able to determine that depending on the location I was coming in from, I was getting no blocks, some blocks or completely blocked. For most of the day I was getting no blocks when I was emerging from a VPN node in Hong Kong. Later in the day, depending on the speed/volume I was hitting at, I was getting about 15/30% blocks. Note that this is using very very basic code, no tricks, no cookie management, and *not* being nice to the TMall server, hammering it in fact.

I took some basic scrape code I have and fed it with the pre-formed URLs Rinal kindly sent me - using this method I was able to successfully scrape 5000 top level search pages in about 14 minutes. I have packaged this data up and sent it to Rinal. Note that I also requested Rinal use the 7ZIP compression utility - this for example allowed me to reduce the storage requirement of the 5k html pages from 110 MBs with standard zip, to 35mb with 7Zip. This massive compression saving has big implications for long term storage of data as previously discussed.

I have cleaned my quick code up (slightly) and it is now capable of looping through even if it hits continuous blocks, until it get the data it needs (this is tenuous and wont last I suspect!). I attach this now - Rinal has seen it working and should be able to use it - hopefully it will help matters in the very short term. As a next step I have promised Rinal to update the code so that it allows configuration of proxy credentials.... I will aim to do this tomorrow (Wed).

As to what is happening with TMall - as I said they are now actively taking steps to block scrapers, and a far more robust strategy than we have now is urgently required to ensure we can keep getting data. Amongst other things, we need to be using a far wider net of proxy IPs, and a far more comprehensive strategy for intelligently changing our 'virtual browser' fingerprint on a constant basis.

I will be in touch again tomorrow as I dig a bit deeper.

regards,

Allen.